Hexacopter Documentation
==========================

For the purpose of 3D potogrammetry based GIS mapping, the requirement at
[ICFOSS](www.icfoss.in) was to develop a Open-Hardware based multirotor.
The hexacopter is based on the [Tarrot 680 Pro](http://www.helipal.com/tarot-fy680-pro-hexacopter-frame-set.html)
frame. The hexacopter (a.k.a night fury) is designed for a total payload of 1.2kg max.
![night fury](images/nightFury.jpg)
>***Night Fury***: inspired from the movie "How to train your dragons"

### Design Characteristics
Particular | Calculation | Values |
-----------|-------------|--------|
**Total Lift Capacity** | 1100g of Thrust per motor * 6 motors = 6600g. Let's take 50% as the maximum lift capacity. [click here for simple thrust calculator ](https://www.omnicalculator.com/other/drone-motor)  | **3300g**
**Power to Weight Ratio** | Total weight of drone shoule be well inside 3Kg and the total trust the drone can deliver is about 6000g. Hence power to weight ratio =>| **2:1**
**Average Curent Draw** | Total weight of drone  = 3000g i.e according to [motor spec sheet](https://gitlab.com/icfoss/drone/hexacopter_docs/tree/master/images/MT3506_Specification2.jpg) each motor will consume a maximum of 4A, that accounts for a total of 24A. Lets say all the other components will consume a maximum of another 3A. | **27A**  
**Estimated Flight Time** | flight times = (Battery Capacity x (max Battery Discharge/100) /Average Amp Draw) x 60 i.e with the 6000mah battery it will be like : (80% of 6A / 27A) x 60 | **10.67 mins**


### Weight distribution
Components          |  Weight  
--------------------|----------
Gimball             |   200g   
NodeMCU Tigger      |    20g   
NightFury Deadload  |  2010g   
LiPo Battery 6000mah|   555g
GoPro Hero4 black   |   150g
**Total**           |**2935g**


### Hardware List
Particulars| spec   |
-----------|--------|
Flight controller| [Pixhawk cube](https://docs.px4.io/en/flight_controller/pixhawk-2.html), with Intel edison in carrier board for future expansion.
GPS| [Here+ RTK GPS](http://ardupilot.org/copter/docs/common-here-plus-gps.html) for 8-10cm GPS accuracy
Remote Controller | [FrSky Taranis X9D Plus](https://www.frsky-rc.com/product/taranis-x9d-plus-2/)16 channel (up to 32 channels) fully programmable controller with audio playback support.
RC reciever | [FrSky x8R](https://www.frsky-rc.com/product/x8r/) 16channel reciever with SBUS for connection with Pixhawk cube.
ESC | [Hobbywing XRotor 40A ESC](https://www.hobbywingdirect.com/products/xrotor-40a-esc?variant=955949541) with ***no BEC***, **Transient current: 60A and Continuous current: 40A,2-6S Lipo**
Motors | [EMAX MT3506 650KV](https://www.emaxmodel.com/emax-multicopter-motor-mt3506.html), which produces 1100gms (thrust) at 13.8 Amps with 14.8v battery using a APC12*3.8 props
Propellers | [APC 12*3.8](https://robu.in/product/orange-hd-propellers-115011x5-0-abs-1cw1ccw-1pair-green/)
Telemetry Module | [RFD 900+](http://store.rfdesign.com.au/rfd-900p-modem/)
Battery | 4 cell LiPo Battery (currently we use 35C Batteries with a capacity of atleast 5000mah )
Gimball| [Tarrot 2 Axis GoPro gimball](http://www.helipal.com/tarot-t-2d-brushless-gimbal-for-gopro-hero-3.html) **Working voltage : DC 7.4V ~ 14.8V (Recommended 12V, three lithium battery), Working current : 200mA-500mA**
Camera | [GoPro Hero 4](https://www.amazon.in/GoPro-HERO4-Action-Camera-Black/dp/B00NIYNUF2) used in 7mp Normal Mode.

## Hexacopter Wiring and Configuration
![pinout of Pixhawk Cube Flight Controller](images/PixhawkCubePorts.jpg)
**FlightController pinout diagram**

#### ESC-Motor-Flight Controller
Each of the 6 motors are controller by the ESC (electronic speed controllers)
which are powered directly from the power distribution board ***( here we use
the base plate of the tarrot 680 pro as the power distribution board)***. The
control lines from the ESC are connected to the **MAIN OUT** of the pixhawk Cube.

![Tarrot 680 Pro base plate](images/powerDistributionBoard.jpg)

The motors are connected to the flight controller in a different manner. The below
attached Motor order diagram will give a better understanding .

![Motor order Diagram](images/Hexacopter_wiring.png)

More details in Motor order and wiring can be found
[here](http://ardupilot.org/copter/docs/connect-escs-and-motors.html).

#### RC Reciever-Flight controller
The RC reciever FrSky x8R has 8 channels with direct pwm outputs and SBUS out
for recieving all the 16 Channels through a single signal line. So the SBUS out
is connected directly to the  RCIN pin near to the MAIN OUT pins. The RCIN pin
will also power the RC Reciever.

#### Power Module-Flight Controller
The power for the flight controller is supplied by the pixhawk [power brick](http://www.proficnc.com/all-products/80-power-module.html). The power
module also measures current. **It can support upto 8S with a continous current
of upto 30 Amps**. There are two output cables from the pwoer brick. One cable
from the power module connects to POWER1 port of the Pixhawk Cube and the other
cable read and black wire is connect to the power distribution board.

#### GPS Module- Flight controller
The GPS module used is HERE+ module based on Ublox M8P with onboard safety switch.
The module is connected to the Flight Controller through the GPS1 port.

### Telemetry module
The telemetry module used here is [RFD900+](http://store.rfdesign.com.au/rfd-900p-modem/). The telemetryis
connected to the Flight controller through the Telem1 port in the pixhawk Cube.

### Gimbal controller-Rc Reciever
The gimball used is a Tarrot T-2D 2 axis gimbal for gopro
camera. It should be reprogrammed so that we can control
it using the RC. It has two modes out of which we have to
[configure it **Stick position mode**](http://ardupilot.org/copter/docs/common-tarot-gimbal.html#configuring-the-gimbal).
![Tarrot gimbal calibration screenShot](images/Tarot_Gimbal_Config.png)

Once you have configured the gimball, now connect the control lines to the RC
reciever channel 6. You cannot connect the signal lines directly from the
Pixhawk Cube, as the auxilary pinout are in 3.3v logic and would require a
logical converter for doing so. Hence we take the singal lines from the RC
reciever itself.

##### How to power the gimball
We are using a LM2596 DC-DC Buck Converter to power the gimball. Even though the
documentation says the gimball supports upto 4S LiPo battery, many forums suggest
a 3S battery or 12v. Using a 7812 proved to be inefficient as it heats up faster
and consumes a lot of power as they the gimbal draws too much curent. Hence we
use a LM2596 DC-DC Buck Converter to power the gimball.

![LM2596 DC-DC Buck_Converter](images/Buck_Converter.jpg)


### NodeMCU-Flight controller     
In order to trigger the on board GoPro camera, we are using an NodeMCU. Here the
NodeMCU works as a Gopro Remote. The NodeMCU scan its input pin for a logic high
which is passed from the pixhawk in the event of a camera trigger. The program
and the circuit details for a NodeMCU based GoPro trigger can be found
[here](https://gitlab.com/icfoss/drone/nodemcu_gopro_trigger#nodemcu-gopro-trigger). The connection and it settings are described clearly in
[this repo](https://gitlab.com/icfoss/drone/nodemcu_gopro_trigger#how-to-connect-it-with-pixhawk-flight-controller).

![NodeMCU Circuit](images/NodeMCUCircuit.png)

### RC input Channel Mapping
The RC channel lines are confirgured such that each channel corresponds to a
particular functionality. We connect the RC reciever and the flight controller
via the SBUS which gives the full functionality of the reciver, i.e 16 Channels

Channel | Function                 |
--------|--------------------------|
CH1     | ROll Input        
CH2     | PITCH Input        
CH3     | THROTTLE Input        
CH4     | YAW Input        
CH5     | FLIGHT MODE Input        
CH6     | GIMBALL Input (Pitch Angle)        
CH7     | CAMERA TRIGGER        
CH8     | RTL        

There is an option to change the channel functions through the full parameters
list in Mission Planner. The Gimball control is done directly from the reciever.
The signla line from the RC reciever is connected directly to the gimball controller.
[(more details)](https://gitlab.com/icfoss/drone/hexacopter_docs#gimbal-controller-rc-reciever)
![RC input Map Mission Planner](images/RCMAP_MPSetup.png)

### PID Values
The default PID gains are meant for the 3DR IRIS although they work on a wide
variety of frames. Still, to get optimal performance you may need to adjust them
which can be done through the Mission Planner’s Config/Tuning | Copter Pids
screen. The screen shot below shows the most important parameters for Roll/Pitch
(yellow), Yaw (orange), Altitude hold (green), Loiter (pink) and Waypoint
navigation (blue).

![Mission Planner PID Setting demo](images/Tuning_CommonThingsToChange.png)

More details of tuning your PID values to get a stable flight is given
[here](http://ardupilot.org/copter/docs/tuning.html). You can also try the
[Auto Tune](http://ardupilot.org/copter/docs/autotune.html) functionality in the
ardupilot firmware.

After days of tuning, we have achived a stable PID values for the Hexacopter. The
screenshot attached below contains the detailed PID values.
![current PID Values](images/CurrentPIDValues.png)


### Backup Parameters
The full parameter list of the Hexacopter can be downloaded from the Full parameter
windows in Mission Planner. The full parameter list of the hexacopter as on
May 28th 2019 can be found [here](https://gitlab.com/icfoss/drone/hexacopter_docs/tree/master/Paramter_Backup/)
in the repo, under the folder Parameter_Backup.
![Mission Planner Full Parameter List](images/FullParameterList.png)

### Motor Specification
The motor we use is Emax MT3506 650kv. The spec sheet is found
[here](https://gitlab.com/icfoss/drone/hexacopter_docs/blob/master/images/MT3506_Specification2.jpg).

### 3D Printed Files
During the course the research project we have made many iteration in frame design
and component related structures. All the 3D printed stl files can be found
[here](https://gitlab.com/icfoss/drone/hexacopter_docs/tree/master/STL%20Files)

![JpoleAntennaStand](images/JPoleAntenna_Stand.png)


## Taranis x9D Plus Configuration
More details of configuration of the taranis x9d plus is maitined in
[this repo](https://gitlab.com/icfoss/drone/frsky-taranis-x9d-documentation)
